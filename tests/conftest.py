import os
import pytest
from driver.driver import Driver


@pytest.fixture(scope="class")
def config_driver():
    abs_file_path = os.path.join(os.path.dirname(__file__), "../config/android.yml")
    appium_driver = Driver(abs_file_path)
    yield appium_driver
    appium_driver.instance.quit()
