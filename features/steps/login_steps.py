from behave import given, when, then


@given(u'a valid user')
def a_valid_user(context):
    context.user = 'a@a.com'


@given(u'a valid password')
def a_valid_pass(context):
    context.password = '123456'


@when(u'I introduce the credentials')
def introduce_credentials(context):
    context.driver.instance.find_element_by_id(
        "enrique.appiumproject:id/email").send_keys(context.user)
    context.driver.instance.find_element_by_id(
        "enrique.appiumproject:id/password").send_keys(context.password)


@when(u'I press the login button')
def press_button(context):
    context.driver.instance.find_element_by_id(
        "enrique.appiumproject:id/email_sign_in_button").click()


@then(u'I enter into the app')
def enter_app(context):
    pass
