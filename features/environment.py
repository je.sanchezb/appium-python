import os
from time import sleep
from driver.driver import Driver


def before_all(context):
    abs_file_path = os.path.join(os.path.dirname(__file__), "../config/android.yml")
    context.driver = Driver(abs_file_path)


def after_all(context):
    sleep(1)
    context.driver.instance.quit()
