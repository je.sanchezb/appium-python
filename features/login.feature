Feature: Login

    Scenario: Login with valid user and pass
        Given a valid user
        And a valid password
        When I introduce the credentials
        And I press the login button
        Then I enter into the app